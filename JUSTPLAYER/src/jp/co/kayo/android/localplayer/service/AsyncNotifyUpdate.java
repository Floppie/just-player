package jp.co.kayo.android.localplayer.service;

/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import java.util.Hashtable;

import jp.co.kayo.android.localplayer.MainActivity2;
import jp.co.kayo.android.localplayer.R;
import jp.co.kayo.android.localplayer.appwidget.AppWidgetHelper;
import jp.co.kayo.android.localplayer.appwidget.ColorSet;
import jp.co.kayo.android.localplayer.consts.SystemConsts;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioAlbum;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioMedia;
import jp.co.kayo.android.localplayer.provider.ContentsUtils;
import jp.co.kayo.android.localplayer.util.Funcs;
import jp.co.kayo.android.localplayer.util.Logger;
import jp.co.kayo.android.localplayer.util.MyPreferenceManager;
import jp.co.kayo.android.localplayer.util.bean.MediaData;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.widget.RemoteViews;

public class AsyncNotifyUpdate extends AsyncTask<Void, Void, Void> {
    private static final String PLAYBACK_FINISHED = "fm.last.android.playbackcomplete";
    private static final String META_CHANGED = "fm.last.android.metachanged";
    private static final String PLAYBACK_STATE_CHANGED = "fm.last.android.playstatechanged";
    private static final String STATION_CHANGED = "fm.last.android.stationchanged";
    private static final String PLAYBACK_ERROR = "fm.last.android.playbackerror";
    private static final String UNKNOWN = "fm.last.android.unknown";

    MyPreferenceManager pref;
    Handler handler;
    MediaData media;
    Context context;
    private static Object _lock = new Object();
    Notification mNotification;

    public AsyncNotifyUpdate(Context context, Handler handler, MediaData media,
            Notification notification) {
        this.context = context;
        this.handler = handler;
        this.pref = new MyPreferenceManager(context);
        this.media = media;
        this.mNotification = notification;
    }

    @Override
    protected Void doInBackground(Void... params) {
        synchronized (_lock) {
            if (media != null) {
                notifyOn();
            }
            else {
                notifyOff();
            }
        }
        return null;
    }

    private void notifyOn() {
        Logger.d("notifyOn");
        String lastSetMediaId = null, lastSetAlbum = null, lastSetTitle = null, lastSetArtist = null;
        long duration = 0;
        Hashtable<String, String> tbl1 = new Hashtable<String, String>(), tbl2 = new Hashtable<String, String>();
        Bitmap bitmap = null;
        try {
            if (media.mediaId > 0) {
                bitmap = Funcs.getAlbumArt(context, media.mediaId, R.drawable.albumart_mp_unknown,
                        tbl1,
                        tbl2);

                if (tbl2.size() == 0) {
                    ContentsUtils.getAlbum(context, new String[] {
                            AudioAlbum.ALBUM, AudioAlbum.ARTIST,
                            AudioAlbum.ALBUM_ART
                    }, tbl1.get(AudioMedia.ALBUM_KEY), tbl2);
                }

                if (Build.VERSION.SDK_INT > 7) {
                    handler.sendMessage(handler.obtainMessage(MediaPlayerService.MSG_GETAUDIOFOCUS,
                            new Object[] {
                                    tbl1, tbl2, bitmap
                            }));
                }
                lastSetMediaId = Long.toString(media.mediaId);
                lastSetAlbum = tbl1.get(AudioMedia.ALBUM);
                lastSetArtist = tbl1.get(AudioMedia.ARTIST);
                lastSetTitle = tbl1.get(AudioMedia.TITLE);
                duration = Funcs.parseLong(tbl1.get(AudioMedia.DURATION));
            } else {
                if (Build.VERSION.SDK_INT > 7) {
                    handler.sendMessage(handler.obtainMessage(MediaPlayerService.MSG_GETAUDIOFOCUS,
                            new Object[] {
                                    tbl1, tbl2, bitmap
                            }));
                }
                lastSetMediaId = "";
                lastSetAlbum = Funcs.trimString(media.getAlbum());
                lastSetArtist = Funcs.trimString(media.getArtist());
                lastSetTitle = Funcs.trimString(media.getTitle());
                duration = media.getDuration();
                tbl1 = new Hashtable<String, String>();
                tbl1.put(AudioMedia.ALBUM, lastSetAlbum);
                tbl1.put(AudioMedia.ARTIST, lastSetArtist);
                tbl1.put(AudioMedia.TITLE, lastSetTitle);
                tbl1.put(AudioMedia.DURATION, Long.toString(duration));

                tbl2 = new Hashtable<String, String>();
                tbl2.put(AudioMedia.ALBUM, lastSetAlbum);
                tbl2.put(AudioMedia.ARTIST, lastSetArtist);
            }

            Notification notification = new Notification(R.drawable.status,
                    lastSetTitle, System.currentTimeMillis());

            notification.when = Long.MIN_VALUE;
            notification.tickerText = lastSetTitle;
            notification.flags = Notification.FLAG_NO_CLEAR
                    | Notification.FLAG_ONGOING_EVENT;

            if (Build.VERSION.SDK_INT >= 11) {
                notification.tickerText = lastSetTitle + "/" + lastSetAlbum;

                handler.sendMessage(handler.obtainMessage(MediaPlayerService.MSG_SETNOTIFICATION,
                        new Object[] {
                                tbl1, tbl2, bitmap, notification
                        }));
            }
            else {
                boolean useControlNotification = pref.useControlNotification();
                if (useControlNotification) {
                    ColorSet colorset = new ColorSet();
                    colorset.load(context);

                    boolean keepNotify = pref.isKeepNotification();
                    int statusview_id = keepNotify?R.layout.statusbar:R.layout.statusbar2;

                    Intent intent = new Intent(context, NotificationService.class);
                    intent.setAction("Click");

                    PendingIntent contentIntent = PendingIntent.getService(context,
                            0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                    notification.tickerText = lastSetTitle + "/" + lastSetAlbum;

                    RemoteViews remoteView = new RemoteViews(context.getPackageName(),
                            statusview_id);
                    remoteView.setBitmap(R.id.imageView1, "setImageBitmap", bitmap);
                    remoteView.setTextViewText(R.id.textTitle,
                            Funcs.trimString(tbl1.get(AudioMedia.TITLE)));
                    remoteView.setTextViewText(R.id.textArtist,
                            Funcs.trimString(tbl1.get(AudioMedia.ARTIST)));
                    remoteView.setTextViewText(R.id.textTime, Funcs
                            .makeTimeString(Funcs.parseLong(tbl1
                                    .get(AudioMedia.DURATION)), false));
                    
                    if (statusview_id == R.layout.statusbar) {
                        remoteView.setInt(R.id.btnPlay, "setImageResource",
                                R.drawable.widget_pause);
                    }

                    int pricolor = colorset.getColor(ColorSet.KEY_NOTIFI_PRI_COLOR);
                    int seccolor = colorset.getColor(ColorSet.KEY_NOTIFI_SEC_COLOR);
                    if (pricolor != -1) {
                        remoteView.setTextColor(R.id.textTitle, pricolor);
                    }
                    if (seccolor != -1) {
                        remoteView.setTextColor(R.id.textArtist, seccolor);
                        remoteView.setTextColor(R.id.textTime, seccolor);
                    }

                    notification.contentIntent = contentIntent;
                    notification.contentView = remoteView;
                }
                else {
                    Intent i = new Intent(context, MainActivity2.class);
                    i.setAction(SystemConsts.MAIN_ACITON_SHOWHOMW);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                    PendingIntent contentIntent = PendingIntent.getActivity(context,
                            0, i, 0);
                    notification = new Notification(R.drawable.status, lastSetTitle,
                            System.currentTimeMillis());
                    notification.setLatestEventInfo(context, lastSetTitle, lastSetAlbum + " - "
                            + lastSetArtist, contentIntent);
                }

                handler.sendMessage(handler.obtainMessage(MediaPlayerService.MSG_SETNOTIFICATION,
                        new Object[] {
                                tbl1, tbl2, bitmap, notification
                        }));

            }
            Logger.d("notify on update success");
        } finally {
            String geturl = null;
            try {
                geturl = StreamCacherServer.getContentUri(context, media.data);
                Logger.d("geturl=" + geturl);
            } catch (Exception e) {
                Logger.e("getContentUri", e);
                geturl = null;
            }

            // album, artist, title;
            pref.putString("lastSetMediaId", lastSetMediaId);
            pref.putString("lastSetTitle", lastSetTitle);
            pref.putString("lastSetArtist", lastSetArtist);
            pref.putString("lastSetAlbum", lastSetAlbum);
            pref.commit();

            handler.sendMessage(handler.obtainMessage(MediaPlayerService.MSG_UPDATEWIDGET,
                    new Object[] {
                            lastSetMediaId,
                            lastSetTitle,
                            lastSetArtist,
                            lastSetAlbum,
                            media.getDuration(),
                            geturl,
                            AppWidgetHelper.FLG_PLAY
                    }));

            startScrobbler(context, pref, lastSetTitle, lastSetArtist, lastSetAlbum,
                    duration);
        }
    }

    private void notifyOff() {
        Logger.d("notifyOff");
        // service.instantSave();
        if (mNotification != null && mNotification.contentView != null) {

            mNotification.contentView.setInt(R.id.btnPlay,
                    "setImageResource", R.drawable.widget_play);

            handler.sendMessage(handler.obtainMessage(MediaPlayerService.MSG_RELEASEAUDIOFOCUS,
                    mNotification));
        }
        String lastSetMediaId = pref.getStr("lastSetMediaId", "");
        String lastSetTitle = pref.getStr("lastSetTitle", "");
        String lastSetArtist = pref.getStr("lastSetArtist", "");
        String lastSetAlbum = pref.getStr("lastSetAlbum", "");

        handler.sendMessage(handler.obtainMessage(MediaPlayerService.MSG_UPDATEWIDGET,
                new Object[] {
                        lastSetMediaId,
                        lastSetTitle,
                        lastSetArtist,
                        lastSetAlbum,
                        (long) 0,
                        null,
                        AppWidgetHelper.FLG_STOP
                }));

        stopScrobbler(context, pref);
    }

    public static void stopScrobbler(Context context, MyPreferenceManager pref) {
        boolean b = pref.useLastFM();
        if (b) {
            try {
                Intent i = new Intent(PLAYBACK_FINISHED);
                context.sendBroadcast(i);
            } catch (Exception e) {
                Logger.e("stopScrobbler:" + PLAYBACK_FINISHED, e);
                pref.setUseLastFM(false);
            }
        }
    }

    public static void startScrobbler(Context context, MyPreferenceManager pref, String title,
            String artist, String album,
            long duration) {
        boolean b = pref.useLastFM();
        if (b) {
            try {
                Intent i = new Intent(META_CHANGED);
                i.putExtra("artist", artist);
                i.putExtra("track", title);
                if (album != null)
                    i.putExtra("album", album);
                i.putExtra("duration", duration);
                context.sendBroadcast(i);
            } catch (Exception e) {
                Logger.e("startScrobbler:" + META_CHANGED, e);
                pref.setUseLastFM(false);
            }
        }
    }

    public static void pauseScrobbler(Context context, long duration) {
        Intent i = new Intent(META_CHANGED);
        // i.putExtra("artist", artist);
        // i.putExtra("track", title);
        // if(album!=null)
        // i.putExtra("album", album);
        i.putExtra("duration", duration);
        context.sendBroadcast(i);
    }
}
