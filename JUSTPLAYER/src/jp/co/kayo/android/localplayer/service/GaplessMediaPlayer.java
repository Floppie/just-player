
package jp.co.kayo.android.localplayer.service;

/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import java.io.IOException;

import jp.co.kayo.android.localplayer.consts.SystemConsts;
import jp.co.kayo.android.localplayer.fx.AudioFx;
import jp.co.kayo.android.localplayer.util.Logger;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnBufferingUpdateListener;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.os.Build;
import android.preference.PreferenceManager;

public class GaplessMediaPlayer implements OnCompletionListener, OnBufferingUpdateListener,
        OnPreparedListener, OnErrorListener {
    private static class MyMediaPlayer extends MediaPlayer {
        boolean prepared = false;

    }
    
    private AudioFx mAudiofx;
    private int audioSessionId = -1;
    private MyMediaPlayer mMediaPlayer;
    private MyMediaPlayer mSubMediaPlayer;
    private Context mContext;
    private SharedPreferences mPref;
    private boolean isLoop;
    private OnErrorListener mOnErrorListener;
    private OnPreparedListener mOnPreparedListener;
    private OnCompletionListener mOnCompletionListener;
    private OnBufferingUpdateListener mOnBufferingUpdateListener;

    @TargetApi(16)
    public GaplessMediaPlayer(Context context) {
        this.mContext = context;
        this.mPref = PreferenceManager.getDefaultSharedPreferences(mContext);
    }

    @SuppressLint("NewApi")
    private MyMediaPlayer createMediaPlayer() {
        MyMediaPlayer mp = null;
        mp = new MyMediaPlayer();
        mp.setAudioStreamType(AudioManager.STREAM_MUSIC);
        mp.setOnCompletionListener(this);
        mp.setOnBufferingUpdateListener(this);
        mp.setOnCompletionListener(this);
        mp.setOnPreparedListener(this);
        mp.setOnErrorListener(this);
        mp.setLooping(isLoop);
        if(Build.VERSION.SDK_INT > 8){
            if(audioSessionId == -1){
                audioSessionId = mp.getAudioSessionId();
            }else{
                mp.setAudioSessionId(audioSessionId);
            }
        }
        return mp;
    }

    public MyMediaPlayer getMediaPlayer() {
        if (mMediaPlayer != null) {
            return mMediaPlayer;
        }
        else {
            mMediaPlayer = createMediaPlayer();
            return mMediaPlayer;
        }
    }

    public void save() {
        if (mMediaPlayer != null) {
            if (mAudiofx != null) {
                try {
                    mAudiofx.save();
                } catch (Exception e) {
                }
            }
        }
    }

    @SuppressLint("NewApi")
    public void release() {
        destoryAudioFx();
        if (mMediaPlayer != null) {
            mMediaPlayer.release();
            mMediaPlayer = null;
        }
        if (mSubMediaPlayer != null) {
            mSubMediaPlayer.release();
            mSubMediaPlayer = null;
        }
    }

    private void createAudioFx(MediaPlayer mp) {
        if (Build.VERSION.SDK_INT > 8) {
            if (mAudiofx == null) {
                try {
                    mAudiofx = new AudioFx(mContext);
                    mAudiofx.load(mp);
                } catch (Exception e) {
                    Logger.e("AudioFXの生成に失敗", e);
                    mAudiofx = null;
                }
            }
        }
    }
    
    private AudioFx getAudioFx(MediaPlayer mp){
        if(mp!=null){
            createAudioFx(mp);
        }
        return mAudiofx;
    }

    private void destoryAudioFx() {
        if (mAudiofx != null) {
            mAudiofx.release();
            mAudiofx = null;
        }
    }

    public void setOnPreparedListener(OnPreparedListener l) {
        mOnPreparedListener = l;
    }

    public void setOnCompletionListener(OnCompletionListener l) {
        mOnCompletionListener = l;
    }

    public void setOnBufferingUpdateListener(OnBufferingUpdateListener l) {
        mOnBufferingUpdateListener = l;
    }

    public void setOnErrorListener(OnErrorListener l) {
        mOnErrorListener = l;
    }

    public boolean isPlaying() {
        return getMediaPlayer().isPlaying();
    }

    public int getCurrentPosition() {
        return getMediaPlayer().getCurrentPosition();
    }

    public void setLooping(boolean b) {
        isLoop = b;
        getMediaPlayer().setLooping(b);
    }

    public void start() {
        getMediaPlayer().start();
        createAudioFx(getMediaPlayer());
    }

    public void pause() {
        getMediaPlayer().pause();
    }

    public void stop() {
        getMediaPlayer().stop();
    }

    public void reset() {
        getMediaPlayer().reset();
    }

    @SuppressLint("NewApi")
    public int getAudioSessionId() {
        return getMediaPlayer().getAudioSessionId();
    }

    public void prepareAsync() {
        getMediaPlayer().prepared = false;
        getMediaPlayer().prepareAsync();
    }
    
    public boolean hasNextPlayer(){
        return mSubMediaPlayer != null;
    }

    @SuppressLint("NewApi")
    public boolean startNext() {
        if (mSubMediaPlayer != null) { 
            MyMediaPlayer mp = mMediaPlayer;
            mMediaPlayer = mSubMediaPlayer;
            mSubMediaPlayer = null;
            if (Build.VERSION.SDK_INT < 16 && mMediaPlayer.prepared) {
                mMediaPlayer.start();
            }
            if (mp != null) {
                mp.release();
                mp = null;
            }
            return true;
        } else {
            return false;
        }
    }

    @SuppressLint("NewApi")
    public void setDataSource(String path) throws IllegalArgumentException, SecurityException,
            IllegalStateException, IOException {
        getMediaPlayer().setDataSource(path);
        if (mSubMediaPlayer != null) {
            mSubMediaPlayer.release();
            mSubMediaPlayer = null;
            if (Build.VERSION.SDK_INT >= 16) {
                getMediaPlayer().setNextMediaPlayer(null);
            }
        }
    }

    @SuppressLint("NewApi")
    public void setDataSource(Context context, Uri uri) throws IllegalArgumentException,
            SecurityException, IllegalStateException, IOException {
        getMediaPlayer().setDataSource(context, uri);
        if (mSubMediaPlayer != null) {
            mSubMediaPlayer.release();
            mSubMediaPlayer = null;
            if (Build.VERSION.SDK_INT >= 16) {
                getMediaPlayer().setNextMediaPlayer(null);
            }
        }
    }

    @SuppressLint("NewApi")
    public void setNextDataSource(String path) throws IllegalArgumentException, SecurityException,
            IllegalStateException, IOException {
        if (mSubMediaPlayer != null) {
            mSubMediaPlayer.release();
            mSubMediaPlayer = null;
            if (Build.VERSION.SDK_INT >= 16) {
                getMediaPlayer().setNextMediaPlayer(null);
            }
        }
        if(path!=null && mPref.getBoolean(SystemConsts.KEY_GAPLESS, true)){
            mSubMediaPlayer = createMediaPlayer();
            mSubMediaPlayer.setDataSource(path);
            mSubMediaPlayer.prepared = false;
            mSubMediaPlayer.prepareAsync();
            if (Build.VERSION.SDK_INT >= 16) {
                getMediaPlayer().setNextMediaPlayer(mSubMediaPlayer);
            }
        }
    }

    @SuppressLint("NewApi")
    public void setNextDataSource(Context context, Uri uri) throws IllegalArgumentException,
            SecurityException, IllegalStateException, IOException {
        if (mSubMediaPlayer != null) {
            mSubMediaPlayer.release();
            mSubMediaPlayer = null;
            if (Build.VERSION.SDK_INT >= 16) {
                getMediaPlayer().setNextMediaPlayer(null);
            }
        }
        if(uri!=null && mPref.getBoolean(SystemConsts.KEY_GAPLESS, true)){
            mSubMediaPlayer = createMediaPlayer();
            mSubMediaPlayer.setDataSource(context, uri);
            mSubMediaPlayer.prepared = false;
            mSubMediaPlayer.prepareAsync();
            if (Build.VERSION.SDK_INT >= 16) {
                getMediaPlayer().setNextMediaPlayer(mSubMediaPlayer);
            }
        }
    }

    public int getDuration() {
        return getMediaPlayer().getDuration();
    }

    public void seekTo(int msec) {
        getMediaPlayer().seekTo(msec);
    }

    public void setVolume(float vol, float vol2) {
        getMediaPlayer().setVolume(vol, vol2);
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        if (mp == mMediaPlayer) {
            if (mOnCompletionListener != null) {
                mOnCompletionListener.onCompletion(mp);
            }
        }
    }

    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        if (mp == mMediaPlayer) {
            if (mOnErrorListener != null) {
                mOnErrorListener.onError(mp, what, extra);
            }
        }
        return false;
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        ((MyMediaPlayer) mp).prepared = true;
        if (mp == mMediaPlayer) {
            if (mOnPreparedListener != null) {
                mOnPreparedListener.onPrepared(mp);
            }
        }
    }

    @Override
    public void onBufferingUpdate(MediaPlayer mp, int percent) {
        if (mp == mMediaPlayer) {
            if (mOnBufferingUpdateListener != null) {
                mOnBufferingUpdateListener.onBufferingUpdate(mp, percent);
            }
        }
    }

    public void setEqEnabled(boolean enabled) {
        if (getAudioFx(mMediaPlayer) != null) {
            mAudiofx.setEqEnabled(enabled);
            try {
                mAudiofx.save();
            } catch (Exception e) {
            }
        }
    }

    public boolean getEqEnabled() {
        if (getAudioFx(mMediaPlayer) != null) {
            return mAudiofx.getEqEnabled();
        }
        return false;
    }

    public void setRvEnabled(boolean enabled) {
        if (getAudioFx(mMediaPlayer) != null) {
            mAudiofx.setRvEnabled(enabled);
            try {
                mAudiofx.save();
            } catch (Exception e) {
            }
        }
    }

    public boolean getRvEnabled() {
        if (getAudioFx(mMediaPlayer) != null) {
            return mAudiofx.getRvEnabled();
        }
        return false;
    }

    public void setBsEnabled(boolean enabled) {
        if (getAudioFx(mMediaPlayer) != null) {
            mAudiofx.setBsEnabled(enabled);
            try {
                mAudiofx.save();
            } catch (Exception e) {
            }
        }
    }

    public boolean getBsEnabled() {
        if (getAudioFx(mMediaPlayer) != null) {
            return mAudiofx.getBsEnabled();
        }
        return false;
    }

    public void setVrEnabled(boolean enabled) {
        if (getAudioFx(mMediaPlayer) != null) {
            mAudiofx.setVrEnabled(enabled);
            try {
                mAudiofx.save();
            } catch (Exception e) {
            }
        }
    }

    public boolean getVrEnabled() {
        if (getAudioFx(mMediaPlayer) != null) {
            return mAudiofx.getVrEnabled();
        }
        return false;
    }
    
    
    public int getNumberOfPresets() {
        if (getAudioFx(mMediaPlayer) != null) {
            return mAudiofx.getNumberOfPresets();
        }
        return 0;
    }

    public String getPresetName(short n) {
        if (getAudioFx(mMediaPlayer) != null) {
            return mAudiofx.getPresetName(n);
        }
        return null;
    }

    public int getBandLevel(short band) {
        if (getAudioFx(mMediaPlayer) != null) {
            return mAudiofx.getBandLevel(band);
        }
        return 0;
    }

    public int getCenterFreq(short band) {
        if (getAudioFx(mMediaPlayer) != null) {
            return mAudiofx.getCenterFreq(band);
        }
        return 0;
    }

    public int getCurrentPreset() {
        if (getAudioFx(mMediaPlayer) != null) {
            return mAudiofx.getCurrentPreset();
        }
        return 0;
    }

    public int getNumberOfBands() {
        if (getAudioFx(mMediaPlayer) != null) {
            return mAudiofx.getNumberOfBands();
        }
        return 0;
    }

    public int getMinEQLevel() {
        if (getAudioFx(mMediaPlayer) != null) {
            return mAudiofx.getMinEQLevel();
        }
        return 0;
    }

    public int getMaxEQLevel() {
        if (getAudioFx(mMediaPlayer) != null) {
            return mAudiofx.getMaxEQLevel();
        }
        return 0;
    }

    public void usePreset(short preset) {
        if (getAudioFx(mMediaPlayer) != null) {
            mAudiofx.usePreset(preset);
        }
    }

    public void setBandLevel(short band, short level) {
        if (getAudioFx(mMediaPlayer) != null) {
            mAudiofx.setBandLevel(band, level);
        }
    }

    public int getRvPreset() {
        if (getAudioFx(mMediaPlayer) != null) {
            return mAudiofx.getRvPreset();
        }
        return 0;
    }

    public void setRvPreset(short preset) {
        if (getAudioFx(mMediaPlayer) != null) {
            mAudiofx.setRvPreset(preset);
        }
    }

    public int getStrength() {
        if (getAudioFx(mMediaPlayer) != null) {
            return mAudiofx.getStrength();
        }
        return 0;
    }

    public void setStrength(short strength) {
        if (getAudioFx(mMediaPlayer) != null) {
            mAudiofx.setStrength(strength);
        }
    }
    
    public int getVirtual() {
        if (getAudioFx(mMediaPlayer) != null) {
            return mAudiofx.getVirtual();
        }
        return 0;
    }

    public void setVirtual(short virtual) {
        if (getAudioFx(mMediaPlayer) != null) {
            mAudiofx.setVirtual(virtual);
        }
    }
}
