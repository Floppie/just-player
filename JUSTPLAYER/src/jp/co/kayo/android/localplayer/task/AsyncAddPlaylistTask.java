
package jp.co.kayo.android.localplayer.task;

/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import java.util.List;

import jp.co.kayo.android.localplayer.consts.MediaConsts;
import jp.co.kayo.android.localplayer.consts.SystemConsts;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioMedia;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioPlaylistMember;
import jp.co.kayo.android.localplayer.dialog.AddPlaylistDialog;
import jp.co.kayo.android.localplayer.util.bean.PlaylistInfoLoader;
import jp.co.kayo.android.localplayer.util.bean.PlaylistInfoLoader.CallType;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;

public class AsyncAddPlaylistTask extends AsyncTask<Void, Void, Void> {
    private Context context;
    private FragmentManager fm;
    private long[] ids;
    private String where;
    private String[] whereArgs;
    private Uri uri;
    private String cname;
    private String order;
    private CallType callType;
    
    public AsyncAddPlaylistTask(Context context, FragmentManager fm, String where, String[] whereArgs, CallType type){
        this.context = context;
        this.fm = fm;
        this.where = where;
        this.whereArgs = whereArgs;
        this.order = AudioMedia.TRACK;
        this.uri = null;
        this.ids = null;
        this.callType = type;
    }
    
    public AsyncAddPlaylistTask(Context context, FragmentManager fm, String where, String[] whereArgs, String order, CallType type){
        this.context = context;
        this.fm = fm;
        this.where = where;
        this.whereArgs = whereArgs;
        this.order = order;
        this.uri = null;
        this.ids = null;
        this.callType = type;
    }

    public AsyncAddPlaylistTask(Context context, FragmentManager fm, Uri uri, String cname, String where, String[] whereArgs, String order, CallType type) {
        this.context = context;
        this.fm = fm;
        this.uri = uri;
        this.cname = cname;
        this.where = where;
        this.whereArgs = whereArgs;
        this.order = order;
        this.ids = null;
        this.callType = type;
    }
    
    public AsyncAddPlaylistTask(Context context, FragmentManager fm, long[] ids, CallType type){
        this.context = context;
        this.fm = fm;
        this.where = null;
        this.whereArgs = null;
        this.order = null;
        this.uri = null;
        this.ids = ids;
        this.callType = type;
    }
    
    public AsyncAddPlaylistTask(Context context, FragmentManager fm, List<Long> ids, CallType type){
        this.context = context;
        this.fm = fm;
        this.where = null;
        this.whereArgs = null;
        this.order = null;
        this.uri = null;
        this.ids = new long[ids.size()];
        for(int i=0; i<ids.size(); i++){
            this.ids[i] = ids.get(i);
        }
        this.callType = type;
    }
    
    
    @Override
    protected Void doInBackground(Void... params) {
        Cursor cursor = null;
        try {
            if(ids == null){
                if(uri!=null){
                    cursor = context.getContentResolver().query(uri,
                            new String[]{cname}, where, whereArgs, order);
                }
                else if(where!=null){
                    cname = AudioMedia._ID;
                    cursor = context.getContentResolver().query(
                            MediaConsts.MEDIA_CONTENT_URI,
                            new String[]{cname},
                            where,
                            whereArgs, order);
                }
                if (cursor != null && cursor.moveToFirst()) {
                    int n = cursor.getCount();
                    if (n > 0) {
                        ids = new long[n];
                        int i = 0;
                        do {
                            long id = cursor.getLong(0);
                            ids[i] = id;
                            i++;
                        } while (cursor.moveToNext());
                    }
                }
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void result) {
        if(ids!=null){
            AddPlaylistDialog dlg = AddPlaylistDialog.createAddPlaylistDialog(ids, callType);
            dlg.show(fm, SystemConsts.TAG_RATING_DLG);
        }
    }
}
